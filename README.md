# README #

Forecast iOS App - Exercise

## Getting Started
This application shows you the current and the next 5 days weather in a simple interface.
The background image change based on weather conditions.
You can swipe the bottom bar to see the forecast hourly up to next 5 days.

### Prerequisites

MacOS, XCode 8+, Swift 3, iOS 8.0+, GIT (installed on your local machine)

### Installing

Run the following command on your Terminal.

```
git clone https://Celes13@bitbucket.org/Celes13/forecast_app.git
```
this will make a local copy of the remote repository.
After that open the ForecastApp.xcodeproj project with XCode.

## Running

Click 'Run' or use shortcut 'Cmd + R' on XCode to run the app.
Yuo can use any iPhone Simulator with iOS 8.0+

## Author

* **Marco Celestino**

### Next steps

- Minor fix
- Possibility to choose the city.
- Save your favourite cities.
- Set the favourite unit of measure (Kelvin, Celsius, Fahrenheit)
- More detailed information
- Widget
- Localizable Strings
