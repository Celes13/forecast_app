//
//  NetworkManager.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

public let NetworkingErrorDomain = "\(Bundle.main.bundleIdentifier!).NetworkingError"
public let MissingHTTPResponseError: Int = 1000
public let UnexpectedResponseError: Int = 2000
public typealias JSON = [String : Any]

class NetworkManager{
    let request: URLRequest
    lazy var configuration: URLSessionConfiguration = URLSessionConfiguration.default
    lazy var session: URLSession = URLSession(configuration: self.configuration)
    
    init(request: URLRequest) {
        self.request = request
    }
    
    ///JSON typealias closure
    typealias JSONHandler = (JSON?, HTTPURLResponse?, Error?) -> Void
    ///Data typealias closure
    typealias DataHandler = (Data?, HTTPURLResponse?, Error?) -> Void
    
    /**
    *   This function is for download and parse a JSON.
    *
    *   @param completion The escaping closure of the function.
    *
    *   @return Void
    */
    func downloadJSON(completion: @escaping JSONHandler){
        let dataTask = session.dataTask(with: self.request) { (data, response, error) in
            guard let httpResponse = response as? HTTPURLResponse else {
                let userInfo = [NSLocalizedDescriptionKey : NSLocalizedString("Missing HTTP Response", comment: "")]
                let error = NSError(domain: NetworkingErrorDomain, code: MissingHTTPResponseError, userInfo: userInfo)
                completion(nil, nil, error as Error)
                return
            }
            
            if data == nil {
                if let error = error {
                    completion(nil, httpResponse, error)
                }else{
                    completion(nil, httpResponse, nil)
                }
            } else {
                switch httpResponse.statusCode {
                case 200:
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String : Any]
                        completion(json, httpResponse, nil)
                    } catch let error as NSError {
                        completion(nil, httpResponse, error)
                    }
                default:
                    print("ERROR: \(httpResponse.statusCode) - was not handled")
                }
            }
        }
        
        dataTask.resume()
    }
    
    /**
     *   This function is for download an icon.
     *
     *   @param completion The escaping closure of the function.
     *
     *   @return Void
     */
    func downloadIcon(completion: @escaping DataHandler){
        let dataTask = session.dataTask(with: self.request) { (data, response, error) in
            guard let httpResponse = response as? HTTPURLResponse else {
                let userInfo = [NSLocalizedDescriptionKey : NSLocalizedString("Missing HTTP Response", comment: "")]
                let error = NSError(domain: NetworkingErrorDomain, code: MissingHTTPResponseError, userInfo: userInfo)
                completion(nil, nil, error as Error)
                return
            }
            
            if data == nil {
                if let error = error {
                    completion(nil, httpResponse, error)
                }else{
                    completion(nil, httpResponse, nil)
                }
            } else {
                completion(data, httpResponse, nil)
            }
        }
        dataTask.resume()
    }
}
