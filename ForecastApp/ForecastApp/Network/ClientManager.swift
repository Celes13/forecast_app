//
//  ClientManager.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

class ClientManager {
    
    ///Singleton shared instance of ClientManager
    static let shared = ClientManager()
    
    private init(){
        
    }
    
    /**
     *   This function is for fetch JSON forecast data of a city.
     *
     *   @param cityId The id of the city to fetch.
     *
     *   @param completion The escaping closure of the function.
     *
     *   @return Void
     */
    func getFiveDaysAndThreeHoursForecastFor(cityId: String, completion: @escaping (APIResult<BaseResponse>) -> Void){

        let urlRequest = URLRequest(url: Utility.getUrlFor(endPoint: Utility.EndPoint.fiveDaysEP, andCity: cityId))
        let netManager = NetworkManager(request: urlRequest)
        netManager.downloadJSON { (json, http, error) in
            DispatchQueue.main.async {
                guard let response = json else {
                    if let error = error {
                        completion(.failure(error))
                    }
                    return
                }
                
                completion(.success(BaseResponse(withJsonObject: response)))
            }
        }
    }
    
    /**
     *   This function is for download an icon.
     *
     *   @param conditionCode The code of the icon to download.
     *
     *   @param completion The escaping closure of the function.
     *
     *   @return Void
     */
    func getIconFor(conditionCode code: String, completion: @escaping (APIResult<Data>) -> Void){
        let urlRequest = URLRequest(url: URL(string: String(format: Utility.EndPoint.getIconEPFormat, code))!)
        let netManager = NetworkManager(request: urlRequest)
        netManager.downloadIcon { (data, http, error) in
            DispatchQueue.main.async {
                guard let data = data else {
                    if let error = error {
                        completion(.failure(error))
                    }
                    return
                }
                
                completion(.success(data))
            }
        }
    }
        
}
