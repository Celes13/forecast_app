//
//  APIResult.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

enum APIResult<T> {
    case success(T)
    case failure(Error)
}
