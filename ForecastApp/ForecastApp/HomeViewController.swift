//
//  ViewController.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    ///Default value
    let notAvailable = "Not available"
    
    struct Collection{
        static let collectionCellId = "HourlyCell"
    }
    
    //MARK: IBOutlet
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var degreeLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sectionHeaderLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    
    
    //MARK: Variables
    var baseForecast : BaseResponse?{
        didSet{
            setupUI()
            collectionView.reloadData()
        }
    }
    
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getForecast()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    
    
    //MARK: UI
    
    /**
     *   Set the current weather informations
     *
     *   @return Void
     */
    private func setupUI(){
        if let baseForecast = baseForecast, let todayForecast = baseForecast.foreCastList?[0]{
            backgroundImage.image = Utility.getImageFor(conditionsCode: todayForecast.weathers?[0].id)
            cityLabel.text = baseForecast.city?.name ?? notAvailable
            descriptionLabel.text = todayForecast.weathers?[0].description ?? notAvailable
            degreeLabel.text = " " + Int((todayForecast.temperature?.temperature)!).description + "°"
            humidityLabel.text = String(format: Utility.StringFormat.humidity, todayForecast.temperature?.humidity ?? notAvailable)
            windLabel.text = String(format: Utility.StringFormat.wind, todayForecast.wind?.speed ?? notAvailable)
            pressureLabel.text = String(format: Utility.StringFormat.pressure, todayForecast.temperature?.pressure ?? notAvailable)
            let date = Utility.getDateFrom(string: (baseForecast.foreCastList?[0].dateString)!, withFormat: Utility.dateFormat)
            sectionHeaderLabel.text = date?.toWeekDay()
        }
    }

    
    //MARK: Private Methods
    
    /**
     *   Fetch forecast data from API.
     *
     *   @return Void
     */
    fileprivate func getForecast(){
        ClientManager.shared.getFiveDaysAndThreeHoursForecastFor(cityId: Utility.CityId.Rome) { (result) in
            switch result{
            case .failure(let error):
                let alert = UIAlertView(title: "OPS!", message: "Sorry, there was an error!", delegate: nil, cancelButtonTitle: "Close")
                alert.show()
                print(error.localizedDescription)
            case .success(let response):
                self.baseForecast = response
            }
        }
    }
}

//MARK: Collection View
extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let forecasts = baseForecast?.foreCastList{
            return forecasts.count
        }
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Collection.collectionCellId, for: indexPath) as! HourlyForecastCollectionViewCell
        cell.forecast = baseForecast?.foreCastList?[indexPath.item]
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width : CGFloat = self.collectionView.frame.width / 4
        let height : CGFloat = self.collectionView.frame.height
        let size = CGSize(width: width, height: height)
        return size
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = collectionView.frame.size.width
        let currentPage : Float = Float(collectionView.contentOffset.x / pageWidth)
        if currentPage >= 0{
            if let forecast = baseForecast?.foreCastList?[Int(currentPage * 4)]{
                let date = Utility.getDateFrom(string: forecast.dateString!, withFormat: Utility.dateFormat)
                sectionHeaderLabel.text = date?.toWeekDay()
            }
        }
    }


}

