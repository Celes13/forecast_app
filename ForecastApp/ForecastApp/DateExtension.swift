//
//  DateExtension.swift
//  ForecastApp
//
//  Created by Marco Celestino on 22/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

extension Date {
    
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    
    func toPrettyString(withFormat format: String) -> String {
        let format = DateFormatter.dateFormat(fromTemplate: format, options: 0, locale: Locale.current)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format;
        return dateFormatter.string(from: self)
    }
    
    func toPrettyString() -> String {
        return toPrettyString(withFormat: "MM/dd/YY")
    }
    
    func toClockHour() -> String {
        return toPrettyString(withFormat: "HH:mm")
    }
    
    func toWeekDay() -> String {
        return toPrettyString(withFormat:"EEEE")
    }
    
}
