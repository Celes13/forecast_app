//
//  CollectionViewCell.swift
//  ForecastApp
//
//  Created by Marco Celestino on 22/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import UIKit

class HourlyForecastCollectionViewCell: UICollectionViewCell {
    
    //MARK: IBOutlet
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var conditionIcon: UIImageView!
    @IBOutlet weak var degreeLabel: UILabel!
    
    //MARK: Var
    var forecast : Forecast?{
        didSet{
            updateUI()
        }
    }
    
    //MARK: UI
    fileprivate func updateUI(){
        if let forecast = forecast{
            let date = Utility.getDateFrom(string: forecast.dateString!, withFormat: Utility.dateFormat)
            hourLabel.text = date?.toClockHour()
            degreeLabel.text = " " + Int(forecast.temperature!.temperature!).description + "°"
            ClientManager.shared.getIconFor(conditionCode: forecast.weathers![0].icon!, completion: { (result) in
                switch result{
                case .success(let data):
                    self.conditionIcon.image = UIImage(data: data)
                case .failure( _):
                    break
                }
            })
        }
    }
    
}
