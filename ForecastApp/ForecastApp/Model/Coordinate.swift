//
//  Coordinate.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

class Coordinate : Response{
    //Constant keys
    fileprivate let kLon = "lon"
    fileprivate let kLat = "lat"
    
    //MARK: Variables
    var longitude : Double?
    var latitude : Double?
    
    //MARK: Init
    required init(withJsonObject json: JSON?) {
        longitude = json?[kLon] as? Double
        latitude = json?[kLat] as? Double
    }
}
