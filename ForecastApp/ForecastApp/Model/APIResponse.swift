//
//  APIResponse.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

protocol Response {
    init(withJsonObject json: JSON?)
}
class APIResponse : Response{
    
    var response : BaseResponse?
    
    required init(withJsonObject json: JSON?) {
        response = BaseResponse(withJsonObject: json)
    }
}
