//
//  Clouds.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

class Clouds: Response {
    //Contant keys
    fileprivate let kAll = "all"
    
    //MARK: Variables 
    var all : Int?
    
    //MARK: Init
    required init(withJsonObject json: JSON?) {
        all = json?[kAll] as? Int
    }
}
