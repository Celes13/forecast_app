//
//  Weather.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

class Forecast: Response {
    //Constant keys
    fileprivate let kDt = "dt"
    fileprivate let kMain = "main"
    fileprivate let kWeather = "weather"
    fileprivate let kClouds = "clouds"
    fileprivate let kWind = "wind"
    fileprivate let kDtTxt = "dt_txt"
    
    //MARK: Variables
    var dateTime : Int?
    var temperature : Temperature?
    var weathers : [Weather]?
    var clouds : Clouds?
    var wind : Wind?
    var dateString : String?
    
    //MARK: Init
    required init(withJsonObject json: JSON?) {
        dateTime = json?[kDt] as? Int
        temperature = Temperature(withJsonObject: json?[kMain] as? JSON)
        if let weathersDicts = json?[kWeather] as? [JSON]{
            weathers = weathersDicts.map { json in
                return Weather(withJsonObject: json)
            }
        }
        clouds = Clouds(withJsonObject: json?[kClouds] as? JSON)
        wind = Wind(withJsonObject: json?[kWind] as? JSON)
        dateString = json?[kDtTxt] as? String
    }
}
