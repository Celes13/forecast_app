//
//  City.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

class City : Response{
    //Constant keys
    fileprivate let kId = "id"
    fileprivate let kName = "name"
    
    //MARK: Variables
    var id : Int?
    var name : String?
    
    //MARK: Init
    required init(withJsonObject json: JSON?) {
        id = json?[kId] as? Int
        name = json?[kName] as? String
    }
}
