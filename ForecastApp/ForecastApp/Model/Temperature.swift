//
//  Temperature.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

class Temperature: Response {
    
    //Constant keys
    fileprivate let kTemp = "temp"
    fileprivate let kTempMin = "temp_min"
    fileprivate let kTempMax = "temp_max"
    fileprivate let kPressure = "pressure"
    fileprivate let kSeaLevel = "sea_level"
    fileprivate let kGrndLevel = "gnrd_level"
    fileprivate let kHumidity = "humidity"
    fileprivate let kTempKf = "temp_kf"
    
    //MARK: Variables
    var temperature : Double?
    var minTemperature : Double?
    var maxTemperature : Double?
    var pressure : Double?
    var seaLevel : Double?
    var gnrdLevel : Double?
    var humidity : Int?
    var tempKF : Double?
    
    //MARK: Init
    required init(withJsonObject json: JSON?) {
        temperature = json?[kTemp] as? Double
        minTemperature = json?[kTempMin] as? Double
        maxTemperature = json?[kTempMax] as? Double
        pressure = json?[kPressure] as? Double
        seaLevel = json?[kSeaLevel] as? Double
        gnrdLevel = json?[kGrndLevel] as? Double
        humidity = json?[kHumidity] as? Int
        tempKF = json?[kTempKf] as? Double
    }
}
