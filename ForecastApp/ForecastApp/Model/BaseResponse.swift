//
//  Forecast.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

class BaseResponse : Response{
    
    //Constant keys
    fileprivate let kCity = "city"
    fileprivate let kCoordinate = "coord"
    fileprivate let kCountry = "country"
    fileprivate let kCode = "cod"
    fileprivate let kMessage = "message"
    fileprivate let kCnt = "cnt"
    fileprivate let kList = "list"
    
    //MARK: Variables
    var city : City?
    var coordinate : Coordinate?
    var country : String?
    var code : String?
    var message : Double?
    var cnt : Int?
    var foreCastList : [Forecast]?
    
    //MARK: Init
    required init(withJsonObject json: JSON?) {
        city = City(withJsonObject: json?[kCity] as? JSON)
        coordinate = Coordinate(withJsonObject: json?[kCoordinate] as? JSON)
        country = json?[kCountry] as? String
        code = json?[kCode] as? String
        message = json?[kMessage] as? Double
        cnt = json?[kCnt] as? Int
        if let forecasts = json?[kList] as? [JSON]{
            foreCastList = forecasts.map { forecast in
                return Forecast(withJsonObject: forecast)
            }
        }
    }
}
