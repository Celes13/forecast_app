//
//  Wind.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

class Wind : Response {
    //Contant keys
    fileprivate let kSpeed = "speed"
    fileprivate let kDeg = "deg"
    
    //MARK: Variables
    var speed : Double?
    var deg : Double?
    
    //MARK: Init
    required init(withJsonObject json: JSON?) {
        speed = json?[kSpeed] as? Double
        deg = json?[kDeg] as? Double
    }
}
