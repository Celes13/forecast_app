//
//  Weather.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation

class Weather : Response{
    //Constant keys
    fileprivate let kId = "id"
    fileprivate let kMain = "main"
    fileprivate let kDesc = "description"
    fileprivate let kIcon = "icon"
    
    //MARK: Variables
    var id : Int?
    var main : String?
    var description : String?
    var icon : String?
    
    //MARK: Init
    required init(withJsonObject json: JSON?) {
        id = json?[kId] as? Int
        main = json?[kMain] as? String
        description = json?[kDesc] as? String
        icon = json?[kIcon] as? String
    }
}
