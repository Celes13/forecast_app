//
//  Utility.swift
//  ForecastApp
//
//  Created by Marco Celestino on 21/07/2017.
//  Copyright © 2017 Celes. All rights reserved.
//

import Foundation
import UIKit

class Utility{
    
    static let apiKey = "8706a92db2090e7938f3264e9aa2cacd"
    static let dateFormat = "yyyy/MM/dd HH:mm:ss"
    
    struct EndPoint{
        static let fiveDaysEP = "http://api.openweathermap.org/data/2.5/forecast?id="
        static let getIconEPFormat = "http://openweathermap.org/img/w/%@.png"
    }
    
    struct CityId{
        static let Rome = "3169070"
    }
    
    enum DegreeUnit : String{
        case kelvin = ""
        case celsius = "metric"
        case fahrenheit = "imperial"
    }
    
    static func getUrlFor(endPoint: String, andCity cityId: String) -> URL{
        return URL(string: "\(endPoint)\(cityId)&appid=\(Utility.apiKey)&units=\(DegreeUnit.celsius.rawValue)")!
    }
    
    static func getImageFor(conditionsCode code: Int?) -> UIImage{
        if let code = code{
            switch code {
            case 200..<300:
                return #imageLiteral(resourceName: "thunderstorm")
            case 300..<400:
                return #imageLiteral(resourceName: "drizzle")
            case 500..<600:
                return #imageLiteral(resourceName: "rain")
            case 600..<700:
                return #imageLiteral(resourceName: "snow")
            case 700..<800:
                return #imageLiteral(resourceName: "fog")
            case 800:
                return #imageLiteral(resourceName: "clear")
            case 801..<900:
                return #imageLiteral(resourceName: "cloud")
            default:
                return #imageLiteral(resourceName: "default")
            }
        }else{
            return #imageLiteral(resourceName: "default")
        }
    }
    
    static func getDateFrom(string: String, withFormat format: String) -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: string)
    }
    
    struct StringFormat{
        static let humidity = "Humidity: %d%%"
        static let wind = "Wind speed: %.1f m/sec"
        static let pressure = "Pressure: %.1f hPa"
    }
}

