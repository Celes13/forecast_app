# Project Title

Forecast iOS App - Exercise

## Getting Started

### Prerequisites

XCode 8+, Swift 3, GIT (on your local machine)

### Installing

Run the following command on your Terminal.

```
git clone https://Celes13@bitbucket.org/Celes13/forecast_app.git
```

after that open the ForecastApp.xcodeproj project.

## Running

Click 'Run' or use shortcut 'Cmd + R' on XCode to run the app.

## Author

* **Marco Celestino**
